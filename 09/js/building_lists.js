
var request = new XMLHttpRequest();
request.open('GET', 'data/books.json', false);
request.send(null);
var data = JSON.parse(request.responseText);
console.log(data);

var books = data.books;



var list = document.createElement('table');
list.innerHTML = "<tr><td>Name</td><td>Year</td><td>ISBN</td><td>Authors</td></tr>";
for (var i=0; i < books.length; i++) {
	var row = document.createElement('tr');
	var c1 = document.createElement("td");
	var c2 = document.createElement("td");
	var c3 = document.createElement("td");
	var c4 = document.createElement("td");
	c1.innerHTML = books[i].title;
	c2.innerHTML = books[i].year;
	c3.innerHTML = books[i].isbn;
	for( var j = 0; j < books[i].authors.length; ++j ){
		c4.innerHTML += (j ? ", " : "") + books[i].authors[j];
	}
	row.appendChild(c1);
	row.appendChild(c2);
	row.appendChild(c3);
	row.appendChild(c4);
	row.setAttribute("custom-title", books[i].title);
	row.onclick = function(e){
		document.getElementById("top_of_the_page").innerHTML = e.target.parentElement.getAttribute("custom-title");
	};
	list.appendChild(row);
}
document.body.innerHTML = "<h1 id=\"top_of_the_page\"></h1>";
document.body.appendChild(list);
