"use strict";

window.onload = function loginlogout() {
    var ca = document.cookie.split(";");
    
    if (ca.length >= 1 && ca[ca.length-1] != "") {
        for (var i = 0; i<ca.length; i++) {
            if (ca[i].search("sess_key") != -1) {
                document.getElementById("loglabel").style.display = "none";
                document.getElementById("logoutlabel").style.display = "initial";
                
            }
        }
        
    }
    
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
      if (xmlhttp.readyState == 4 && xmlhttp.status == 200 ) {
          var cardres = JSON.parse(xmlhttp.responseText);
          var bodtxt = document.createTextNode(cardres.body);
          document.getElementById("cardbody").appendChild(bodtxt);
          document.getElementById("mailpic").setAttribute("action", "mailto:"+cardres.mail);
      }
    };
    xmlhttp.open("POST", "https://cdx-server.com/v6k/static.php?load_card", true);
    xmlhttp.send();
    
};

function logout() {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
      if (xmlhttp.readyState == 4 && xmlhttp.status == 200 ) {
          //alert("logout");
          document.cookie = "sess_key"+"=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;";
		  location.reload();
      }
    };
    xmlhttp.open("POST", "https://cdx-server.com/v6k/user.php?action=logout", true);
    xmlhttp.send();
}

function loginUser() {
    
    var url = "https://cdx-server.com/v6k/user.php?action=login&name=";
    var x = document.getElementsByName("uname");
    var uname = x[0].value;
    var y = document.getElementsByName("psw");
    var upass = y[0].value;
    url = url+uname+"&password="+upass;
	
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
      if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
		  //alert("vastaus: "+xmlhttp.status+"\n"+xmlhttp.responseText);
          var ans = JSON.parse(xmlhttp.responseText);
          if (ans.result == "ok") {
              document.cookie = "sess_key="+ans.sess_key+"; Path=/;";
              //alert("welcome v6k");
              x[0].value = "";
              y[0].value = "";
              document.getElementById("id01").style.display = "none";
			  location.reload();
          } else {
              alert("loginError");
              x[0].value = "";
              y[0].value = "";
              document.getElementById("id01").style.display = "none";
          }
              
      }
    };
    xmlhttp.open("POST", url, true);
    xmlhttp.send();
    
    
    
}

function recaptchaCallback() {
    document.getElementById("contactbutton").removeAttribute("disabled");
}
