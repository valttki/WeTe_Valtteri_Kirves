"use strict";
var currentInd;
var currentInd2;
var postid;
var hackhack;
window.onload = function blogit() {
    
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            var blogposts = JSON.parse(xmlhttp.responseText);
            for (var j = 0; j < blogposts.length; j++) {
                // blog element setup
				var i = blogposts[j].id;
                var blogs = document.getElementById("blogs");
                var card = document.createElement("div");
                var cardhead = document.createElement("div");
                var cardbody = document.createElement("div");
                var blogtit = document.createElement("h5");
                var blogtext = document.createElement("p");
                var btnframe = document.createElement("p");
                var commentbtn = document.createElement("button");
                var collapsed = document.createElement("div");
                var collnav = document.createElement("nav");
                var navdiv = document.createElement("div");
                var navdivtab1 = document.createElement("a");
                var navdivtab2 = document.createElement("a");
                var divtablist = document.createElement("div");
                var divtab1 = document.createElement("div");
                var divtab2 = document.createElement("div");
                var form = document.createElement("form");
                var formdiv = document.createElement("div");
                var linebreak = document.createElement("br");
                var inputnick = document.createElement("input");
                var inputcomment = document.createElement("textarea");
                var submitbtn = document.createElement("button");
                
                // element attributes setup
                card.setAttribute("class", "card");
                cardhead.setAttribute("class", "card-header");
				cardhead.setAttribute("name", "blogcardheadx");
                cardhead.setAttribute("id", "blogcardhead"+i);
                cardbody.setAttribute("class", "card-body");
                blogtit.setAttribute("class", "card-title");
                blogtext.setAttribute("class", "card-text");
                commentbtn.setAttribute("class", "btn btn-primary");
                commentbtn.setAttribute("type", "button");
                commentbtn.setAttribute("data-toggle", "collapse");
                commentbtn.setAttribute("data-target", "#collapseIT"+i);
                commentbtn.setAttribute("aria-expanded", "false");
                commentbtn.setAttribute("aria-controls", "collapseIT"+i);
				commentbtn.setAttribute("id", "cmtbtn"+i);
				commentbtn.setAttribute("onclick", "getPostId(this.id); loadComments();");
                collapsed.setAttribute("class", "collapse");
                collapsed.setAttribute("id", "collapseIT"+i);
                navdiv.setAttribute("class", "nav nav-tabs");
                navdiv.setAttribute("id", "nav-tab");
                navdiv.setAttribute("role", "tablist");
                navdivtab1.setAttribute("class", "nav-item nav-link active");
                navdivtab1.setAttribute("id", "nav-comments-tab"+i);
                navdivtab1.setAttribute("data-toggle", "tab");
                navdivtab1.setAttribute("href", "#nav-comments"+i);
                navdivtab1.setAttribute("role", "tab");
                navdivtab1.setAttribute("aria-controls", "nav-comments"+i);
                navdivtab1.setAttribute("aria-selected", "true");
                navdivtab2.setAttribute("class", "nav-item nav-link");
                navdivtab2.setAttribute("id", "nav-leavecomments-tab"+i);
                navdivtab2.setAttribute("data-toggle", "tab");
                navdivtab2.setAttribute("href", "#nav-leavecomments"+i);
                navdivtab2.setAttribute("role", "tab");
                navdivtab2.setAttribute("aria-controls", "nav-leavecomments"+i);
                navdivtab2.setAttribute("aria-selected", "false");
                divtablist.setAttribute("class", "tab-content");
                divtablist.setAttribute("id", "nav-tabContent");
                divtab1.setAttribute("class", "tab-pane fade show active");
                divtab1.setAttribute("id", "nav-comments"+i);
                divtab1.setAttribute("role", "tabpanel");
                divtab1.setAttribute("aria-labelledby", "nav-comments-tab"+i);
                divtab2.setAttribute("class", "tab-pane fade");
                divtab2.setAttribute("id", "nav-leavecomments"+i);
                divtab2.setAttribute("role", "tabpanel");
                divtab2.setAttribute("aria-labelledby", "nav-leavecomments-tab"+i);
                form.setAttribute("onsubmit", "event.preventDefault(); document.getElementById('id01').style.display='block';");
                form.setAttribute("id", "form"+i);
                formdiv.setAttribute("class", "form-group");
                inputnick.setAttribute("type", "text");
                inputnick.setAttribute("class", "form-control");
                inputnick.setAttribute("placeholder", "Nickname");
                inputnick.setAttribute("id", "nickname"+i);
                inputnick.setAttribute("maxlength", "15");
                inputnick.setAttribute("required", "");
                inputcomment.setAttribute("class", "form-control");
                inputcomment.setAttribute("id", "commentfield"+i);
                inputcomment.setAttribute("rows", "8");
                inputcomment.setAttribute("maxlength", "1000");
                inputcomment.setAttribute("required", "");
                submitbtn.setAttribute("type", "submit");
                submitbtn.setAttribute("class", "btn btn-primary");
                submitbtn.setAttribute("id", "btn"+i);
                submitbtn.setAttribute("onClick", "reply_click(this.id)");
                
                // text setup
                var date = document.createTextNode(blogposts[j].date);
                var title = document.createTextNode(blogposts[j].title);
				var text = document.createElement("span");
				text.innerHTML = blogposts[j].content;
                var btntext = document.createTextNode("Comments");
                var navdivtabtxt1 = document.createTextNode("Show Comments");
                var navdivtabtxt2 = document.createTextNode("Leave Comments");
                var submitbtntxt = document.createTextNode("Leave Comment");
				
                // append textnodes
                cardhead.appendChild(date);
                blogtit.appendChild(title);
                blogtext.appendChild(text);
                commentbtn.appendChild(btntext);
                navdivtab1.appendChild(navdivtabtxt1);
                navdivtab2.appendChild(navdivtabtxt2);
                submitbtn.appendChild(submitbtntxt);
                
                // append elements
                btnframe.appendChild(commentbtn);
                cardbody.appendChild(blogtit);
                cardbody.appendChild(blogtext);
                cardbody.appendChild(btnframe);
                formdiv.appendChild(linebreak);
                formdiv.appendChild(inputnick);
                formdiv.appendChild(inputcomment);
                form.appendChild(formdiv);
                form.appendChild(submitbtn);
                divtab2.appendChild(form);
                divtablist.appendChild(divtab1);
                divtablist.appendChild(divtab2);
                navdiv.appendChild(navdivtab1);
                navdiv.appendChild(navdivtab2);
                collnav.appendChild(navdiv);
                collapsed.appendChild(collnav);
                collapsed.appendChild(divtablist);
                cardbody.appendChild(collapsed);
                card.appendChild(cardhead);
                card.appendChild(cardbody);
                blogs.appendChild(card);
				
            }
			
			var ca = document.cookie.split(";");
    
			if (ca.length >= 1 && ca[ca.length-1] != "") {
				for (var i = 0; i<ca.length; i++) {
					if (ca[i].search("sess_key") != -1) {
						document.getElementById("adminonly").style.display = "initial";
						var blogcards = document.getElementsByName("blogcardheadx");
						for (var i = 0; i <blogcards.length; i++) {
							blogcards[i].setAttribute("onclick", "reply_click2(this.id)");
						}
					}
				}
				
			}
        }
    };
    xmlhttp.open("POST", "https://cdx-server.com/v6k/blog.php?action=load_posts", true);
    xmlhttp.send();
    
    
}

function loadComments() {
    var xmlhttp2 = new XMLHttpRequest();
    xmlhttp2.onreadystatechange = function() {
		if (xmlhttp2.readyState == 4 && xmlhttp2.status == 200 ) {
			var comments = JSON.parse(xmlhttp2.responseText);
			//alert(xmlhttp2.responseText);
			if( comments.length > 0 ){
				var divtab1 = document.getElementById("nav-comments"+comments[0].post_id);
				divtab1.innerHTML = "";
				for (var j = 0; j < comments.length; j++) {
					var container = document.createElement("div");
					container.setAttribute("class", "comment-container");
					container.innerHTML = "["+comments[j].time+"] <b>"+comments[j].nickname+"</b>: "+comments[j].content;
					divtab1.appendChild(container);
				}
			}
		}
    };
    xmlhttp2.open("POST", "https://cdx-server.com/v6k/blog.php?action=load_comments&post_id="+postid, true);
    xmlhttp2.send();
               
}
	

function reply_click(clicked_id) {
    var strippedid = clicked_id.replace("btn", "");
    currentInd = strippedid;
    return currentInd;
}

function reply_click2(clicked_id) {
    var strippedid = clicked_id.replace("blogcardhead", "");
    currentInd2 = strippedid;
    alert(currentInd2);
    return currentInd2;
}

function getPostId(clicked_id) {
	var strippedid = clicked_id.replace("cmtbtn", "");
	postid = strippedid;
	//alert(postid);
	return postid;
}
function recaptchaCallback(asd) {
	hackhack = asd;
    document.getElementById("finalbtn").removeAttribute("disabled");
}

function sendComment() {
    var sendnick = document.getElementById("nickname"+currentInd).value;
    var sendcomm = document.getElementById("commentfield"+currentInd).value;
	
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
      if (xmlhttp.readyState == 4 && xmlhttp.status == 200 ) {
          //alert("thanks"+xmlhttp.responseText);
		  document.getElementById("form"+currentInd).submit();
      }
    };
    xmlhttp.open("POST", "https://cdx-server.com/v6k/blog.php?action=submit_comment&post_id="+currentInd+"&nickname="+encodeURIComponent(sendnick)+"&comment="+encodeURIComponent(sendcomm)+"&captcha="+hackhack, true);
    xmlhttp.send();
    
}

function postnew() {
    var newtit = document.getElementById("newtit").value;
    var newpost = document.getElementById("newpost").value;
    
    
    
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
      if (xmlhttp.readyState == 4 && xmlhttp.status == 200 ) {
          console.log("new blog post");
		  var forms = document.getElementsByClassName("adminonlypriv");
		  forms[0].submit();
      }
    };
    xmlhttp.open("POST", "https://cdx-server.com/v6k/blog.php?action=submit_post&title="+encodeURIComponent(newtit)+"&content="+encodeURIComponent(newpost), true);
    xmlhttp.send();
    
    
}


function postedit() {
    var edid = document.getElementById("editid").value;
    var edtit = document.getElementById("editit").value;
    var edpost = document.getElementById("editpost").value;
    
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
      if (xmlhttp.readyState == 4 && xmlhttp.status == 200 ) {
          console.log("post editet");
		  var forms = document.getElementsByClassName("adminonlypriv");
		  forms[1].submit();
      }
    };
    xmlhttp.open("POST", "https://cdx-server.com/v6k/blog.php?action=edit_post&post_id="+edid+"&title="+encodeURIComponent(edtit)+"&content="+encodeURIComponent(edpost), true);
    xmlhttp.send();
    
}

function deletepost() {
    var delid = document.getElementById("delid").value;
    
    
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
      if (xmlhttp.readyState == 4 && xmlhttp.status == 200 ) {
          console.log("post deleted");
		  var forms = document.getElementsByClassName("adminonlypriv");
		  forms[2].submit();
      }
    };
    xmlhttp.open("POST", "https://cdx-server.com/v6k/blog.php?action=delete_post&post_id="+delid, true);
    xmlhttp.send();
    
}

function deletecomment() {
    var delid2 = document.getElementById("delid2").value;
    
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
      if (xmlhttp.readyState == 4 && xmlhttp.status == 200 ) {
          console.log("comment deleted");
		  var forms = document.getElementsByClassName("adminonlypriv");
		  forms[3].submit();
      }
    };
    xmlhttp.open("POST", "https://cdx-server.com/v6k/blog.php?action=delete_comment&comment_id="+delid2, true);
    xmlhttp.send();
    
}