<?php
	/* Config */
	$_cfg['mysql_user'] = "v6k";
	$_cfg['mysql_pw'] = "UFDB3gSnZDRb4TIM";
	$_cfg['mysql_db'] = "v6k";
	$_cfg['recaptcha_secret'] = "6LePPCkUAAAAAIuJe9uzTPbJpmu1ap3vU1BPzN8k";
	
	/* Hash pw */
	function v6k_pwhash($pw){
		return hash("sha512", $pw."adjfweofekefooedfjaojcwfeojqwedfjqqpwf");
	}
	
	/* JSON exit */
	function v6k_json($data){
		exit(json_encode($data));
	}
	
	/* ReCaptcha validation */
	function v6k_recaptcha($response){
		global $_cfg;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, array('secret' => $_cfg['recaptcha_secret'], 'response' => $response));
		$resp_raw = curl_exec($ch);
		curl_close($ch);
		if( ($json = json_decode($resp_raw, true)) ){
			if( $json['success'] == "true" ){
				return true;
			}
		}
		return false;
	}
	
	/* Connect to database */
	$_sql = new mysqli("localhost", $_cfg['mysql_user'], $_cfg['mysql_pw'], $_cfg['mysql_db']);
	if( $_sql->connect_errno ) exit("MySQL connection error");
	if( !$_sql->set_charset("utf8") ) exit("MySQL charset error");
?>