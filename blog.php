<?php
	require_once "common.php";
	require_once "auth.php";

	/*
	
			LOAD POSTS
	
	*/
	if( $_GET['action'] == "load_posts" ){
		$posts = array();
		$q = $_sql->query("SELECT * FROM `posts` ORDER BY `id` DESC");
		while( $r = $q->fetch_assoc() ){
			$r['date'] = date("d-m-Y", $r['timestamp']);
			$r['content'] = str_replace("\n", "<br>", $r['content']);
			$posts[] = $r;
		}
		v6k_json($posts);
	
	/*
	
			LOAD COMMENTS
	
	*/
	}else if( $_GET['action'] == "load_comments" ){
		$comments = array();
		$q = $_sql->query("SELECT `id` AS `comment_id`, `post_id`, `timestamp` AS `time`, `author_name` AS `nickname`, `content` FROM `comments` WHERE `post_id` = '".intval($_GET['post_id'])."'");
		while( $r = $q->fetch_assoc() ){
			$r['nickname'] = htmlspecialchars($r['nickname']);
			$r['content'] = htmlspecialchars($r['content']);
			$r['time'] = date("d-m-Y", $r['time']);
			$comments[] = $r;
		}
		v6k_json($comments);
		
		
	/*
	
			SUBMIT COMMENT
	
	*/
	}else if( $_GET['action'] == "submit_comment" ){
		/* safety checks */
		if( empty($_GET["nickname"]) || empty($_GET["comment"]) ) v6k_json(array('error' => "empty comment and or nickname"));
		if( !v6k_recaptcha($_GET['captcha']) ) v6k_json(array('error' => "invalid captcha"));
			
		/* insert */
		$_sql->query("
			INSERT INTO `comments` (`post_id`, `timestamp`, `author_name`, `author_ip`, `content`) VALUES (
				'".intval($_GET['post_id'])."',
				'".time()."',
				'".$_sql->real_escape_string($_GET['nickname'])."',
				'".ip2long($_SERVER['REMOTE_ADDR'])."',
				'".$_sql->real_escape_string($_GET['comment'])."'
			)
		");
		
		/* ok */
		v6k_json(array('ok' => "ok"));
		
	/*
	
	
			SUBMIT POST
	
	
	*/
	}else if( $_GET['action'] == "submit_post" ){
		/* auth check */
		if( !$_auth ) v6k_json(array('error' => "unauthorized"));
		
		/* insert */
		$_sql->query("
			INSERT INTO `posts` (`author`, `timestamp`, `title`, `content`) VALUES (
				'".$_auth['id']."',
				'".time()."',
				'".$_sql->real_escape_string($_GET['title'])."',
				'".$_sql->real_escape_string($_GET['content'])."'
			)
		");
		
		/* ok */
		v6k_json(array('ok' => "ok"));
		
		
	/*
	
			EDIT POST
	
	
	*/
	}else if( $_GET['action'] == "edit_post" ){
		/* auth check */
		if( !$_auth ) v6k_json(array('error' => "unauthorized"));
		
		/* edit */
		$_sql->query("
			UPDATE	`posts`
			SET		`title` = '".$_sql->real_escape_string($_GET['title'])."',
					`content` = '".$_sql->real_escape_string($_GET['content'])."',
					`timestamp_modified` = '".time()."'
			WHERE	`id` = '".intval($_GET['post_id'])."'
		");
		
		/* ok */
		v6k_json(array('ok' => "ok"));
		
		
	/*
	
	
			DELETE POST
		
	
	*/
	}else if( $_GET['action'] == "delete_post" ){
		/* auth check */
		if( !$_auth ) v6k_json(array('error' => "unauthorized"));
		
		/* delete */
		$_sql->query("DELETE FROM `posts` WHERE `id` = '".intval($_GET['post_id'])."' LIMIT 1");
		
		/* ok */
		v6k_json(array('ok' => "ok"));
		
	
	/*
	
	
			DELETE COMMENT
	
	
	*/
	}else if( $_GET['action'] == "delete_comment" ){
		/* auth check */
		if( !$_auth ) v6k_json(array('error' => "unauthorized"));
		
		/* delete */
		$_sql->query("DELETE FROM `comments` WHERE `id` = '".intval($_GET['comment_id'])."' LIMIT 1");
		
		/* ok */
		v6k_json(array('ok' => "ok"));
	}
	
?>