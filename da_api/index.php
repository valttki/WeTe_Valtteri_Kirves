<?php
    $_persons = array();
    $raw = file_get_contents("db");
    foreach( explode("\n", $raw) as $row ){
        $data = explode("|", $row);
        if( count($data) == 3 ){
            $_persons[] = array("id" => $data[0], "firstname" => $data[1], "lastname" => $data[2]);
        }
    }
    
    function write_data(){
        global $_persons;
        $first = true;
        $output = "";
        foreach( $_persons as $p ){
            if( $p['id'] != "delete" ){
                if( $first ) $first = false; else $output .= "\n";
                $output .= $p['id']."|".$p['firstname']."|".$p['lastname'];
            }
        }
        file_put_contents("db", $output);
    }
    
# URI parser helper functions
# ---------------------------

    function getResource() {
        # returns numerically indexed array of URI parts
        $resource_string = $_SERVER['REQUEST_URI'];
        if (strstr($resource_string, '?')) {
            $resource_string = substr($resource_string, 0, strpos($resource_string, '?'));
        }
        $resource = array();
        $resource = explode('/', $resource_string);
        array_shift($resource);   
        return $resource;
    }

    function getParameters() {
        # returns an associative array containing the parameters
        return $_GET;
    }

    function getMethod() {
        # returns a string containing the HTTP method
        $method = $_SERVER['REQUEST_METHOD'];
        return $method;
    }
 
# Handlers
# ------------------------------
# These are mock implementations

	function postPerson($parameters) {
		# implements POST method for person
		# Example: POST /staffapi/person/id=13&firstname="John"&lastname="Doe"
		$firstname=urldecode($parameters["firstname"]);
		$lastname=urldecode($parameters["lastname"]);
		echo "Posted ".$parameters["id"]." ".$firstname." ".$lastname;
		
		global $_persons;
		$_persons[] = array("id" => $parameters['id'], "firstname" => $firstname, "lastname" => $lastname);
		write_data();
	}

	function getPersons() {
		# implements GET method for persons (collection)
		# Example: GET /staffapi/persons
		echo "Getting list of persons";
		global $_persons;
		foreach( $_persons as $p ){
		    echo $p['id'].", ".$p['firstname'].", ".$p['lastname']."<br>";
		}
	}

	function getPerson($id) {
		# implements GET method for person 
		# Example: GET /staffapi/person/13
		echo "Getting person: ".$id;
		global $_persons;
		foreach( $_persons as $p ){
		    if($p['id'] == $id){
		         echo $p['id'].", ".$p['firstname'].", ".$p['lastname']."<br>";
		    }
		}
	}

	function deletePerson($id) {
		# implements DELETE method for person 
		# Example: DELETE /staffapi/person/13
		echo "Deleting person: ".$id;
		global $_persons;
		foreach( $_persons as &$p ){
		    if($p['id'] == $id){
		         $p['id'] = "delete";
		         break;
		    }
		}
		write_data();
	}

# Main
# ----

	$resource = getResource();
    $request_method = getMethod();
    $parameters = getParameters();
    var_dump($parameters);
    
    # Redirect to appropriate handlers.
	if ($resource[0]=="da_api") {
    	if ($request_method=="POST" && $resource[1]=="person") {
        	postPerson($parameters);
    	}
		else if ($request_method=="GET" && $resource[1]=="persons") {
			getPersons();
		} 
		else if ($request_method=="GET" && $resource[1]=="person") {
			getPerson($resource[2]);
		}
		else if ($request_method=="DELETE" && $resource[1]=="person") {
			deletePerson($resource[2]);
		}
		else {
			http_response_code(405); # Method not allowed
		}
	}
	else {
		http_response_code(405); # Method not allowed
	}
?>

