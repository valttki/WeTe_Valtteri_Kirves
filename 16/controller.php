 <?php

    class Controller {

        private $model;



        public function __construct() {

            $this->model = new Model();

        }



        public function list_it() {

            $this->messages = $this->model->messages();

            include("view.php");

        }



        public function send() {

            $this->model->add_message($_POST["message"], $_POST['nick']);
            
            setcookie("nick", $_POST['nick']);
            
            header("Location: chat.php?action=list_it");

        }

    }

    ?>