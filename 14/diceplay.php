<?php

include("diceclasses.inc.php");

$faces = $_GET["faces"];
$throws = $_GET["throws"];
$bias = isset($_GET["bias"]) ? $_GET['bias'] : null;

$results = array();

// make dice
$dice = null;
if( isset($_GET['material']) ){
    $dice = new PhysicalDice($faces, $bias, $_GET['material']);
}else{
    $dice = new Dice($faces, $bias);
}

for ($i = 1; $i<=$throws; $i++) {
    $res = $dice->cast();
    $results[] = array('id' => strval($i), 'res' => strval($res));
}
$freqs = array();
for ($i = 1; $i<=$faces; $i++) {
    $freqs[] = array ('eyes' => strval($i), 'frequency' => strval($dice->getFreq($i)));
}
echo json_encode(array('faces'=>$faces,'results'=>$results,'frequencies'=>$freqs, 'avg' => $dice->avg(), 'material' => (isset($_GET['material']) ? $dice->getMaterial() : "unknown")));



?>