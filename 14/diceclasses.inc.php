<?php

class Dice {
    private  $faces;
    private  $freqs = array();
    private  $bias;
    
    // Constructor
    public function __construct($faces, $bias) {
        $this->faces = $faces;
        $this->bias = $bias;
    }
    
    public function cast() {
        $res = 0;
        if( $this->bias !== null ){
            $randfloat = mt_rand() / mt_getrandmax();
            if( $randfloat <= $this->bias ){
                $res = $this->faces;
            }else{
                $res = rand(1,$this->faces-1);
            }
        }else{
            $res = rand(1,$this->faces);
        }
        $this->freqs[$res]++;
        return $res;
    }
    
    public function getFreq($eyes) {
        $freq = $this->freqs[$eyes];
        if ($freq=="")
            $freq = 0;
        return $freq;
    }
    
    public function avg(){
        $sum = 0;
        $cunt = 0;
        foreach( $this->freqs as $k => $v ){
            $sum += $k * $v;
            $cunt += $v;
        }
        return $sum / $cunt;
    }
}

class PhysicalDice extends Dice {
    private $material;
    
    public function __construct($faces, $bias, $material){
        parent::__construct($faces, $bias);
        $this->material = $material;
    }
    
    public function getMaterial(){
        return $this->material;
    }
}

?>