<?php
	/*
		POST-parametrit:
		action 			joko "login" tai "logout"
		name
		password
		
		Login palauttaa:
		{
			"result" : joko "ok" tai "error"
			"sess_key" : sessioavain
		}
	*/
	require_once "common.php";
	require_once "auth.php";
	
	/*
	
			LOGIN
	
	*/
	if( $_GET['action'] == "login" ){
		$q = $_sql->query("
			SELECT	`id`
			FROM	`users`
			WHERE	`name` = '".$_sql->real_escape_string($_GET['name'])."'
			AND		`password` = '".v6k_pwhash($_GET['password'])."'
			LIMIT	1
		");
		if( $q && ($r = $q->fetch_assoc()) ){
			$sess_key = md5(uniqid());
			$_sql->query("
				UPDATE	`users`
				SET		`sess_ip` = '".ip2long($_SERVER['REMOTE_ADDR'])."',
						`sess_key` = '".$sess_key."'
				WHERE	`id` = '".$r['id']."'
				LIMIT	1
			");
			v6k_json(array('result' => "ok", 'sess_key' => $sess_key));
		}else{
			v6k_json(array('result' => "error"));
		}
	
	/*
	
			LOGOUT
	
	*/
	} else if( $_GET['action'] == "logout" ) {
		if( $_auth ){
			$_sql->query("UPDATE `users` SET `sess_key` = NULL WHERE `id` = '".$_auth['id']."' LIMIT 1");
		}		
	}
	
	/*
	
			GEN ADMIN
	
	*/
	/*}else if( $_GET['action'] == "gen_admin" ){
		$_sql->query("INSERT INTO `users` (`name`, `password`) VALUES ('v6k', '".v6k_pwhash("loller0")."')");
	}*/
	
	
?>